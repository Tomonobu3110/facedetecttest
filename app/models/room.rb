class Room < ApplicationRecord
  validates :pi_id, { presence: true, uniqueness: true }
  validates :roomr, { presence: true }

  def created_at_JST
    self.created_at.in_time_zone('Tokyo').strftime('%Y-%m-%d %H:%M:%S')
  end
end
