class Face < ApplicationRecord
  validates :pi_id, { presence: true }
  validates :number, { presence: true }
  
  def get_room
    r = Room.find_by(pi_id: self.pi_id)
    return r ? r.roomr : "no name"
  end
  
  def created_at_JST
    self.created_at.in_time_zone('Tokyo').strftime('%Y-%m-%d %H:%M:%S')
  end
  
  def created_at_JST_simple
    self.created_at.in_time_zone('Tokyo').strftime('%m/%d %H:%M')
  end
end
