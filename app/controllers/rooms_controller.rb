class RoomsController < ApplicationController
  def index
    @rooms = Room.all.order(:pi_id)
  end

  def show
    @room = Room.find_by(pi_id: params[:pi_id])
  end
  
  def new
    @room = Room.new
  end

  def edit
    @room = Room.find_by(id: params[:id])
  end
  
  def create
    @room = Room.new(
      pi_id: params[:pi_id],
      roomr: params[:roomr]
    )
    if @room.save
      flash[:notice] = "部屋の名前を登録しました"
      redirect_to("/rooms/index")
    else
      flash[:notice] = "登録エラー"
      render("/rooms/new")
    end
  end

  def update
    # update values
    @room = Room.find_by(id: params[:id])
    @room.roomr = params[:roomr]

    # save
    if @room.save
      flash[:notice] = "部屋の名前を編集しました"
      redirect_to("/rooms/index")
    else
      render("rooms/edit")
    end
  end
  
  def assign
    pi_id = -1
    room = Room.find_by(roomr: params[:room])
    if !room.nil?
      pi_id = room.pi_id
    else
      Room.connection.execute(
        "INSERT INTO rooms (pi_id, roomr, created_at, updated_at) SELECT COALESCE(max(pi_id), 0)+1, '#{params[:room]}', '#{Time.now}', '#{Time.now}' from rooms"
      )
      pi_id = Room.find_by(roomr: params[:room]).pi_id
#     (error handling)
#     flash[:notice] = "登録に失敗しました"
#     redirect_to("/rooms/new") # TBD
    end
    redirect_to("/rooms/#{pi_id}")
  end

end
