class FacesController < ApplicationController
  def index
    @faces = Face.find_by_sql(['select * from faces as m where m.number >= 0 and not exists (select 1 from faces as s where m.pi_id = s.pi_id and m.created_at < s.created_at) order by pi_id;'])
  end

  def detail
    @faces = Face.find_by_sql(['select * from faces as m where not exists (select 1 from faces as s where m.pi_id = s.pi_id and m.created_at < s.created_at) order by pi_id;'])
  end

  def show
    @faces = Face.where(pi_id: params[:pi_id]).order(created_at: "DESC")

    # グラフ表示用
    faces = @faces.map{ |f| [ Time.parse(f.created_at.in_time_zone('Tokyo').strftime('%Y-%m-%d %H:%M:%S')).to_i * 1000 , f.number.to_i ] }
    # グラフ（チャート）を作成 
    @chart = LazyHighCharts::HighChart.new("graph") do |c|
      c.chart(type: 'column', zoomType: 'x', panning: true, panKey: 'shift')
      c.plotOptions(
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        },
        column: {
            pointWidth: 2,
            maxPointWidth: 10,
            pointPadding: 0.2,
            borderWidth: 0
        }
      )
      c.title(text: "#{@faces.first.get_room}の利用人数履歴")
      c.xAxis(
        type: 'datetime', 
        title: { text: '年月日'},
        dateTimeLabelFormats: {
            second: '%m/%d<br/>%H:%M:%S',
            minute: '%m/%d<br/>%H:%M',
            hour:   '%m/%d<br/>%H:%M',
            day:    '%Y年<br/>%m月%d日',
            week:   '%Y年<br/>%m月%d日',
            month:  '%Y年<br/>%m月',
            year:   '%Y年'
        }
      )
      c.yAxis(title: { text: '人数' });
      c.series(name: "人数", data: faces)
      #c.legend(enabled: false)
    end
  end
  
  def weekly
    # param (YYYYMMDD format)
    ymd = params[:yyyymmdd]
    
    # room name
    @room = Room.find_by(pi_id: params[:pi_id])
    @pi_id = params[:pi_id]
    
    # date/time creation for specified week
    if ymd
      this_day = DateTime.parse(ymd)
    else
      this_day = Date.today
    end

    @week_day = Array.new(5)
    (0..4).each do |w|
      @week_day[w] = this_day - (this_day.wday - 1 - w)
    end

    # query (group by every 10 min)
    query = "select max(substr(long_timestamp, 12, 4)) as ts, count(number) as cnt, sum(number) as sum, min(number) as min, max(number) as max from faces "
    query = query + "where pi_id = ? and substr(long_timestamp, 1, 10) = ? group by substr(long_timestamp, 1, 15);"
    faces = Array.new(5)
    (0..4).each do |w|
      faces[w] = Face.find_by_sql([query, params[:pi_id], @week_day[w].strftime('%Y-%m-%d')])
    end

    # aggregation by each 30 min
    @cnt = Array.new(5) { Array.new }
    @sum = Array.new(5) { Array.new }
    @min = Array.new(5) { Array.new }
    @max = Array.new(5) { Array.new }
    (0..4).each do |w|
      faces[w].each do |face|
        i = (face.ts[0..1].to_i * 2) + (face.ts[3].to_i < 3 ? 0 : 1)
        @cnt[w][i] = @cnt[w][i].nil? ? face.cnt.to_i : @cnt[w][i] + face.cnt.to_i
        @sum[w][i] = @sum[w][i].nil? ? face.sum.to_i : @sum[w][i] + face.sum.to_i
        @min[w][i] = @min[w][i].nil? ? face.min.to_i : (face.min.to_i < @min[w][i].to_i ? face.min.to_i : @min[w][i].to_i)
        @max[w][i] = (@max[w][i].to_i < face.max.to_i ? face.max.to_i : @max[w][i].to_i)
      end
    end  
  end
  
  def item(week, idx)
    if @cnt[week][idx].to_i > 0 and @sum[week][idx].to_i > 0
      return "<b>ave : " +  format("%.2f", @sum[week][idx].to_f / @cnt[week][idx].to_f) + "</b> max : " + (@max[week][idx]).to_s + " min : " + (@min[week][idx]).to_s
    else
      return ""
    end
  end
  helper_method :item
  
  def new
    @face = Face.new
  end

  def create
    @face = Face.new(
      pi_id: params[:pi_id],
      number: params[:number],
      short_timestamp: params[:short_timestamp],
      long_timestamp: params[:long_timestamp],
      source: params[:source],
      diff: params[:diff],
    )
    if @face.save
      flash[:notice] = "顔の数情報を登録しました"
      redirect_to("/faces/index")
    else
      render("/faces/new")
    end
  end
  
  def delete  
    @count = Face.count
  end
  
  def truncate
    if 7500 < params[:limit].to_i and params[:limit].to_i < Face.count()
      del_number = Face.count() - params[:limit].to_i
      del_record = Face.order(:created_at).limit(del_number)
      del_record.destroy_all
      flash[:notice] = del_number.to_s + "件のレコードを削除し、" + params[:limit] + "件にしました"
    end
    redirect_to("/faces/index")
  end
  
  def truncate_zero
    del_record = Face.where(number: 0).where("long_timestamp < ?", Time.zone.now.beginning_of_day).order(:id).reverse_order
    del_record.destroy_all
    redirect_to("/faces/index")
  end
end
