class AddDiffToFaces < ActiveRecord::Migration[5.0]
  def change
    add_column :faces, :diff, :decimal
  end
end
