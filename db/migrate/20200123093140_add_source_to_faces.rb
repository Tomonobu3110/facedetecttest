class AddSourceToFaces < ActiveRecord::Migration[5.0]
  def change
    add_column :faces, :source, :string
  end
end
