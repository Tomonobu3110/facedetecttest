class AddTimestampToFaces < ActiveRecord::Migration[5.0]
  def change
    add_column :faces, :short_timestamp, :string
    add_column :faces, :long_timestamp, :string
  end
end
