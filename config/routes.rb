Rails.application.routes.draw do
  # home
  get 'home/top'

  # rooms
  get 'rooms/index'
  get 'rooms/new'
  get 'rooms/:pi_id' => "rooms#show"
  get 'rooms/:id/edit' => "rooms#edit"
  post 'rooms/create' => "rooms#create"
  post 'rooms/assign' => "rooms#assign"
  post 'rooms/:id/update' => "rooms#update"

  # faces
  get 'faces/index'
  get 'faces/detail'
  get 'faces/new'
  get 'faces/delete'
  get 'faces/:pi_id/weekly' => "faces#weekly"
  get 'faces/:pi_id/weekly/:yyyymmdd' => "faces#weekly"
  get 'faces/:pi_id' => "faces#show"
  post 'faces/create' => "faces#create"
  post 'faces/truncate' => "faces#truncate"
  post 'faces/truncate_zero' => "faces#truncate_zero"

  # root (redirect to home/top)
  get "/" => "faces#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
